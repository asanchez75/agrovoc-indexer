<?php

/**
 * Use
 * $url = 'http://agropedialabs.iitk.ac.in:8080/agroTagger/resources/agrotags'
 * $voc_type = 'agrotag'
 * $outputtype = 'text/plain'
 * $tags_no = 10
 */
class AgrovocIndexer {
  public static $instance;
  // URL webservice
  private $url;
  // type of vocabulary, can be agrovoc or agrotag
  private $voc_type;
  // type of output, can be xml/rdf, application/JSON, text/plain
  private $outputtype;
  // number of tags, can be 10, 15, 20
  private $tags_no;

  private function __construct($url, $voc_type, $outputtype, $tags_no) {
    $this->url = $url;
    $this->voc_type = $voc_type;
    $this->outputtype = $outputtype;
    $this->tags_no = $tags_no;
  }

  private function __clone() {}

  public static function get_instance($url, $voc_type, $outputtype, $tags_no) {
    if (!(self::$instance instanceof self)) {
      self::$instance = new self($url, $voc_type, $outputtype, $tags_no);
    }
    return self::$instance;
  }

  public function retrieve_agrovoc_tags($filepath) {
    $file = file_get_contents($filepath);
    file_put_contents($filepath, $file);

    $parameters['file_url'] = $filepath;
    $parameters['voc_type'] = $this->voc_type;
    $parameters['outputtype'] = $this->outputtype;
    $parameters['tags_no'] = $this->tags_no;
    $parameters['url'] = $this->url;

    $start = microtime(true);
    $tags = self::curl_request($parameters);
    $time_taken = microtime(true) - $start;
    return self::cleanup_output($tags);
  }

  /**
   * Clean up output
   * @param  [type] $output [description]
   * @return [type]         [description]
   */
  public static function cleanup_output($output) {
    $output = rtrim($output, '<br>');
    $output = substr($output, 19);
    return $output;
  }

  public static function get_tids_from_names($names, $vid = NULL, $save_new = FALSE) {
    $titles = drupal_explode_tags($names);
    foreach ($titles as $title) {
      $query = db_select('taxonomy_term_data', 't');
      $query->fields('t', array('tid'));
      $query->condition('t.name', $title);
      if (!is_null($vid)) {
        $query->condition('t.vid', $vid);
      }
      $tid = $query->execute()->fetchField();
      if (!empty($tid)) {
        $tids[] = $tid;
      }
      elseif ($save_new == TRUE && !is_null($vid)) {
        $tids[] = self::create_taxonomy_term($title, $vid);
      }
      else {
        drupal_set_message(t($title . ' does not exists yet in database and was not stored'), 'status', FALSE);
      }
    }
    return $tids;
  }

  public static function curl_request($parameters){
    $file = $parameters['file_url'];
    $data['file_url'] = "@$file";
    $data['voctype'] = $parameters['voc_type'];
    $data['outputtype'] = $parameters['outputtype'];
    $data['tags_no'] = $parameters['tags_no'];
    $ch = curl_init();
    $result=curl_setopt($ch, CURLOPT_URL, $parameters['url']);
    $result=curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result=curl_setopt($ch, CURLOPT_POST, 1 );
    $result=curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $http_result = curl_exec($ch);
    return $http_result;
  }

  function create_taxonomy_term($name, $vid) {
    $term = new stdClass();
    $term->name = $name;
    $term->vid = $vid;
    taxonomy_term_save($term);
    return $term->tid;
  }

}
